## Description
This module is intended as a back-end function for an HTTP server written in node.js.
It provides a way of verifying that a username and token pair are valid against Azure AD.

## Requirements
This module requires that the http request that was performend contains 2 cookies with data pertinent to the authorization process, these are listed below:
- '**azure_username**':	The username of the account to be verified.
- '**azure_token**':		The bearer token associated with the account to be verified; it is required that this token matches the username and cannot be associated to a different account.

## Inputs
The following inputs are required when calling the ```authorize()``` function in the module, these are as follow:
- '**origin_request**':	    The remote request object.
- '**origin_response**':    The remote response object (this is passed in the output to be used with the callback function).
- '**callback**': 		    The callback function that will be executed when the autorization porcess is completed.
- '**domain**':             Azure tenant email domain for verification, for example ```contoso.com```.

## Outputs
If both requirements are satisfied, the expected response will contain the following data:
 - '**request**':    	The original HTTP request object.
 - '**response**':   	The response object generated in response to the original request, used to perform response actions to the HTTP client.
 - '**callback**':   	A callback function that will be executed when the autorization process is complete.
 - '**domain**':        Azure tenant email domain that has been verified, for example ```contoso.com```.
 - '**username**':   	The username extracted from the 'azure_username' cookie.
 - '**token**':      	The token extracted from the 'azure_token' cookie.
 - '**authorized**': 	A booean flag that signifies if the account has passed the authrization process.
 
## Dependencies
  - '**cookie**':		'>=0.3.1',
  - '**request**':	    '>=2.88.0'
 
## Examples
```
// Importing the module.
const auth = require("azure-token-verify-http");

// Call the function with a callback (in this case a http response is generated and auth status is sent to the client).
auth.authorize(request, response, "contoso.com", function(event){
    event.response.write(JSON.stringify({"authorized": event.authorized, "username": event.username}));
    event.response.end();
});
```

## Links
- Node.js package: [here](https://www.npmjs.com/package/azure-token-verify-http)
- GitLab Repo: [here](https://gitlab.com/tom.bastianello/azure-token-verify-http)