// Request module, used to perform the graph verification reuqest.
const request = require('request');

// Cookie module used to retreive cookies.
const cookie = require('cookie');

// Exposed module function, this requires 3 imputs: request, response and a callback function.
module.exports = {
    authorize: function(origin_request, origin_response, domain, callback){
        // Initial definition of the event object that will be used throughout the autorization process and returned to the callback function.
        var event = {
            "request": origin_request,
            "response": origin_response,
            "callback": callback,
            "domain": domain
        }
        preProcess(event);
    }
}

// Basic data verification, this stage extracts and verifies the accout username and token from their respective cookies.
function preProcess (event) {
    try{
        event = parseCookieData(event);
    }catch (err) {
        event.username = null;
        event.token = null;
    }    
    if (event.username != null && event.token != null) {
        validateUserData(event);
    }else {
        event.authorized = false;
        event.callback(event);
    }
}

// Generic function to parse the username and token cookies.
function parseCookieData (event) {
    var cookies = cookie.parse(event.request.headers.cookie);
    var username = cookies.azure_username;
    if(username != null && username.includes("@")){
        username = username.split("@")[0] + "@" + event.domain;        
    }
    else {
        username += "@" + event.domain;
    }
    var token = cookies.azure_token;
    event.username = username;
    event.token = token;
    return event;
}

// Perform graph verification by calling the /v1.0/me endpoint, that, if successful, should return the username that was sent with the token.
function validateUserData (event) {
    request.get({url: "https://graph.microsoft.com/v1.0/me/", headers: {"Authorization":"Bearer " + event.token, "Content-type":"application/json"}}, function(err,response,body){
        if(body.includes("userPrincipalName") && body.toLowerCase().includes(event.username.toLowerCase())){
            event.authorized = true;
        }else{
            event.authorized = false;
        }
        event.callback(event);
    });
}